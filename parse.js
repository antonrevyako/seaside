let fs = require ('fs');
let cheerio = require('cheerio');
let request = require('request-promise');
let cron = require('cron').CronJob;;

let routines = require('./lib/routines.js');
let db = require('./lib/db.js');

let config = require('./lib/config.js').get(process.env.NODE_ENV);

let mysql = require('promise-mysql');
let connection;
let mysqlConfig = {
    host: config.mysql.host,
    port: config.mysql.port,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
}

let newItemsCounter = 0;
let imageDownloaderIntervalId;

new cron({
  cronTime: config.cron.config,
  onTick: main(),
  start: true
});

async function connectionMaker(){
  if (connection == null || connection.connection.state !== 'authenticated') {
    try{
        connection = await mysql.createConnection(mysqlConfig);
    }catch(err){
        console.error('connection error', err.code);
        return false;
    }
  }
  return true;

}

async function imagesDownload(){
    if(! await connectionMaker()){
        return;
    }

    let images;
    try{
        images = await connection.query('select id, property_ref, uri from property_images WHERE ok IS NULL AND download_date IS NULL ORDER BY RAND() LIMIT '+config.downloader.count);
    }catch(err){
        // clearInterval(imageDownloaderIntervalId);
        console.error('imagesDownload sql error', err.message);
        return;
    }
    if(images.length == 0){
        // clearInterval(imageDownloaderIntervalId);
        console.log('all images has been downloaded');
        return;
    }else{
        if(imageDownloaderIntervalId == null)
            imageDownloaderIntervalId = setInterval(await imagesDownload, config.downloader.interval);
    }
    images.forEach(async (imgItem)=>{
        try{
            console.log("\tdownloading ",imgItem.uri, '->', imgItem.id+'.jpg');
            let img = await request({uri:imgItem.uri, encoding : null});
            console.log("\tdownloading ",imgItem.id+'.jpg', "finished");
            fs.writeFile(config.downloader.dir+imgItem.id+'.jpg',img, async (err)=>{
                if(!err){
                    await connection.query('UPDATE property_images SET ok=TRUE, download_date=NOW() WHERE id='+imgItem.id);
                }else
                    console.error("\twrite error",imgItem.id);
            });
        }catch(err){
            console.error(imgItem.uri, err.message);
            try{
                await connection.query('UPDATE property_images SET ok=FALSE WHERE id='+imgItem.id);
            }catch(err){
                console.error('imagesDownload sql error 2', err.message);
                return;
            }
        }
    });
}

async function main(){
    let propertyRefs, propertyTypes, propertyClasses, propertyStatuses, propertyDistricts, propertyLocations;

    let statisticsByStatus = {date:new Date(), info:{}};
    let statisticsByType = {date:new Date(), info:{}};
    let statisticsByDistrict = {date:new Date(), info:{}};
    let statisticsByLocation = {date:new Date(), info:{}};

    let parseInfo = {
        date: new Date(),
        refsErrorList : [],
        refsNew : [],
        refsUpdated : []
    }

    let xml;

    if(! await connectionMaker()){
        return;
    }

    console.log(new Date(), 'start loading feed');
    try{
        xml = await request(config.parser.uri);
    }catch(err){
        console.error(new Date(), 'feed loading error!');
        return;
    }

    console.log("\t",new Date(), 'total bytes:', xml.length);

    try{
        //loading refs
        propertyRefs = await db.propertyDictionaryGet(connection, 'property', ['updated_at'], 'ref', 'deleted_at IS NULL');

        //loading dictionary
        propertyTypes =  await db.propertyDictionaryGet(connection, 'property_type', ['id', 'class_id']);
        propertyLocations =  await db.propertyDictionaryGet(connection, 'property_location', ['id', 'district_id']);
        propertyClasses = await db.propertyDictionaryGet(connection, 'property_class');
        propertyStatuses = await db.propertyDictionaryGet(connection, 'property_status');
        propertyDistricts = await db.propertyDictionaryGet(connection, 'property_district');
    }catch(err){
        console.error('dictionary loading error', err);
        return;
    }

    let xmlArr = xml.match(/<property([\s\S]*?)<\/property>/gmi);
    console.log("\t",'total records: ', xmlArr.length);


    let $;
    for(let i=0; i< xmlArr.length;
        i++){
        $ = cheerio.load(xmlArr[i],{
            normalizeWhitespace: true,
            xmlMode: true,
            decodeEntities: true
        });

        //flat xml
        info = routines.flatXml($);

        //updating statistics for cache reasons
        if(info.status!= 'Sold' && info.status!= 'Rented'){
            if(statisticsByStatus.info[info.status])
                statisticsByStatus.info[info.status]++;
            else
                statisticsByStatus.info[info.status]=1;

            if(statisticsByType.info[info.type.type])
                statisticsByType.info[info.type.type]++;
            else
                statisticsByType.info[info.type.type]=1;

            if(statisticsByDistrict.info[info.district])
                statisticsByDistrict.info[info.district]++;
            else
                statisticsByDistrict.info[info.district]=1;

            if(statisticsByLocation.info[info.location])
                statisticsByLocation.info[info.location]++;
            else
                statisticsByLocation.info[info.location]=1;
        }

        //if ref is old, but updated
        if(propertyRefs[info.ref]){

            //if ref is old, but updated
            if(info.updated_at>=propertyRefs[info.ref].updated_at){
                parseInfo.refsUpdated.push(info.ref);

                //remove from propertyRefs
                delete(propertyRefs[info.ref]);
            }else{

                //remove from propertyRefs
                delete(propertyRefs[info.ref]);

                //old and not updated ->  and goto next one
                continue;
            }
        }else{
            //new one
            parseInfo.refsNew.push(info.ref);
        }

        //errors in data
        if(info.type.type == '' || info.type.class == '' || info.status == '' || info.district == '' || info.location == ''){
            parseInfo.refsErrorList.push(info.ref);
            continue;
        }

        //adding new values to dictionary
        if(!propertyStatuses[info.status]){
            propertyStatuses = await db.propertyDictionaryAdd(connection, 'property_status', {value:info.status}, propertyStatuses);
        }
        if(!propertyClasses[info.type.class]){
            propertyClasses = await db.propertyDictionaryAdd(connection, 'property_class', {value:info.type.class}, propertyClasses);
        }
        if(!propertyTypes[info.type.type]){
            propertyTypes = await db.propertyDictionaryAdd(connection, 'property_type', {value:info.type.type, class_id:propertyClasses[info.type.class].id}, propertyTypes);
        }
        if(!propertyDistricts[info.district]){
            propertyDistricts = await db.propertyDictionaryAdd(connection, 'property_district', {value:info.district}, propertyDistricts);
        }
        if(!propertyLocations[info.location]){
            propertyLocations = await db.propertyDictionaryAdd(connection, 'property_location', {value:info.location, district_id:propertyDistricts[info.district].id}, propertyLocations);
        }

        if(Array.isArray(info.features))
            info.features = JSON.stringify(info.features);
        else
            info.features = '';

        //parse only needed
        let vals = config.parser.parseKeys.reduce((acc,cur, i)=>{
            let s = cur.split('.');
            let val = info[cur];
            if(s.length == 2)
                val = info[s[0]][s[1]];
            if (val == undefined || val == '')
                val = null;

            if(s[0] == 'status')
                val = propertyStatuses[val].id;
            if(s[0] == 'location'){
                val = propertyLocations[val].id;
            }
            if(s[1] && s[1] == 'type')
                val = propertyTypes[val].id;
            if(val == null)
                return acc + "NULL, ";
            return acc + '"'+val.toString().replace(/"/g, "'")+'", ';
        },'');

        let keys = config.parser.parseKeys.reduce((acc,cur, i)=>{
            let s = cur.split('.');
            let val = cur;
            if(s.length == 2)
                val = s[1];

            return acc +val+", ";
        },'');

        keys = keys.substring(0,keys.length-2);
        vals = vals.substring(0,vals.length-2);

        try{
            let ok = await connection.query('REPLACE INTO property ('+keys+') VALUES ('+vals+')');

            if(ok.affectedRows){
                newItemsCounter++;
                //insert images
                if(info.images.length>0){
                    let imgSql= 'INSERT INTO property_images (property_ref, uri) VALUES ';
                    for(let imgId = 0; imgId<info.images.length; imgId++){
                        imgSql += "("+info.ref+",'"+info.images[imgId]+"'),";
                    }
                    imgSql = imgSql.substring(0,imgSql.length-1);
                    await connection.query(imgSql);
                }
            }
        }catch(err){
            console.error("\t",'ERROR SQL ',info.ref, err.code);
            if(err.fatal){
                console.error("FATAL ERROR OCCURRED, EXIT");
                process.exit(1);
            }
        }
    }

    //mark old items as deleted
    if(Object.keys(propertyRefs).length>0){
        try{
            await connection.query('UPDATE property SET deleted_at = NOW() WHERE ref IN ('+Object.keys(propertyRefs).join(',')+')');
        }catch(err){
            console.error("\t",'ERROR SQL 2',info.ref, err.code);
            if(err.fatal){
                console.error("FATAL ERROR OCCURRED, EXIT");
                process.exit(1);
            }
        }
    }
    fs.writeFileSync(config.parser.cacheDir+'statisticsByStatus.json', JSON.stringify(statisticsByStatus));
    fs.writeFileSync(config.parser.cacheDir+'statisticsByType.json', JSON.stringify(statisticsByType));
    fs.writeFileSync(config.parser.cacheDir+'statisticsByDistrict.json', JSON.stringify(statisticsByDistrict));
    fs.writeFileSync(config.parser.cacheDir+'statisticsByLocation.json', JSON.stringify(statisticsByLocation));

    fs.writeFileSync(config.parser.cacheDir+'propertyTypes.json', JSON.stringify(propertyTypes));
    fs.writeFileSync(config.parser.cacheDir+'propertyClasses.json', JSON.stringify(propertyClasses));
    fs.writeFileSync(config.parser.cacheDir+'propertyStatuses.json', JSON.stringify(propertyStatuses));
    fs.writeFileSync(config.parser.cacheDir+'propertyDistricts.json', JSON.stringify(propertyDistricts));
    fs.writeFileSync(config.parser.cacheDir+'propertyLocations.json', JSON.stringify(propertyLocations));
    fs.writeFileSync(config.parser.cacheDir+'parseInfo.json', JSON.stringify(parseInfo));

    console.log("\t",'inserted ', newItemsCounter,'items');
    console.log('done at ', new Date());

    if(newItemsCounter>0){
        //start image loader
        imagesDownload();
    }
}
