async function propertyDictionaryAdd(connection, tableName, values, list){
    if(Object.keys(values).length == 0 || Object.values(values)[0] == ''){
        return list;
    }
    try{
        let ok = await connection.query('INSERT INTO '+tableName+' ('+Object.keys(values).join(', ')+') VALUES(\''+ Object.values(values).join("','") +'\')');
        let newObject = Object.keys(values).slice(1).reduce((acc,cur, i)=>{acc[cur]=Object.values(values)[i+1];return acc;},{});
        list[Object.values(values)[0]]=Object.assign(newObject, {id:ok.insertId});
    }catch(err){
        console.log(err);
        if(err.fatal){
            console.error("FATAL ERROR OCCURRED, EXIT");
            process.exit(1);
        }
    }
    return list;
}

async function propertyDictionaryGet(connection, tableName, keys = ['id'], key = 'value', where = 'TRUE'){
    return await connection.query('select * from '+tableName+' WHERE '+where)
        .reduce((acc,cur, i)=>{
            acc[cur[key]]={};
            for(let ind=0; ind<keys.length; ind++)
                acc[cur[key]][keys[ind]]=cur[keys[ind]];
            return acc;
        },{});
}


module.exports.propertyDictionaryAdd = propertyDictionaryAdd;
module.exports.propertyDictionaryGet = propertyDictionaryGet;
