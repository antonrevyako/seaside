function flatXml($){
    let result ={};
    $("*").each((i,element)=>{
        if(i==0)
            return;
        let attrKeys = Object.keys($(element).get(0).attribs);
        let tag = $(element).get(0).tagName.toString().trim();
        if(attrKeys.length == 0){
            if($(element).children().length){
                result[tag]=[];
                $(element).children().each((i, child)=>{
                    result[tag].push($(child).text().toString().trim());
                });
            }else
                result[tag]=$(element).text().toString().trim();
        }else{
            result[tag]={};
            result[tag][tag] =  $(element).text().toString().trim();
            for(let i = 0; i< attrKeys.length; i++){
                result[tag][attrKeys[i]] = $(element).get(0).attribs[attrKeys[i]];
            }
        }

    });
    return result;
}

module.exports.flatXml = flatXml;
