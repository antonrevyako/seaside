CREATE TABLE property
(
    price INT(11) NOT NULL,
    currency VARCHAR(3) DEFAULT 'EUR' NOT NULL,
    type SMALLINT(6) NOT NULL,
    bedrooms SMALLINT(6),
    bathrooms SMALLINT(6),
    build_date VARCHAR(16),
    covered INT(11),
    plot INT(11),
    latitude DECIMAL(18,15),
    longitude DECIMAL(18,15),
    features MEDIUMTEXT,
    location SMALLINT(6),
    status SMALLINT(6) NOT NULL,
    description MEDIUMTEXT,
    ref INT(11) PRIMARY KEY NOT NULL,
    suffix VARCHAR(16),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);
CREATE TABLE property_class
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value VARCHAR(32) NOT NULL
);
CREATE TABLE property_district
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value VARCHAR(32) NOT NULL
);
CREATE TABLE property_images
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    property_ref INT(11) NOT NULL,
    uri VARCHAR(255) NOT NULL,
    ok TINYINT(1),
    download_date TIMESTAMP DEFAULT NULL,
    resized TINYINT(1) DEFAULT '0' NOT NULL
);
CREATE TABLE property_location
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value VARCHAR(64) NOT NULL,
    district_id INT(11)
);
CREATE TABLE property_status
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value VARCHAR(32) NOT NULL
);
CREATE TABLE property_type
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    class_id BIGINT(20),
    value VARCHAR(32) NOT NULL
);
CREATE UNIQUE INDEX property_ref_uindex ON property (ref);
CREATE UNIQUE INDEX id ON property_class (id);
CREATE UNIQUE INDEX property_class_value_pk ON property_class (value);
CREATE UNIQUE INDEX id ON property_district (id);
CREATE UNIQUE INDEX property_district_id_uindex ON property_district (id);
CREATE UNIQUE INDEX property_district_value_uindex ON property_district (value);
CREATE UNIQUE INDEX id ON property_images (id);
CREATE UNIQUE INDEX property_images_id_uindex ON property_images (id);
CREATE UNIQUE INDEX property_images_uri_uindex ON property_images (uri);
CREATE UNIQUE INDEX id ON property_location (id);
CREATE UNIQUE INDEX property_location_id_uindex ON property_location (id);
CREATE UNIQUE INDEX property_location_value_district_id_uindex ON property_location (value, district_id);
CREATE UNIQUE INDEX id ON property_status (id);
CREATE UNIQUE INDEX property_status_value_uindex ON property_status (value);
CREATE UNIQUE INDEX id ON property_type (id);
CREATE UNIQUE INDEX property_type_value_uindex ON property_type (value);
